package mx.jalan.SHP.app;

import java.util.Arrays;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import mx.jalan.SHP.models.Reporte;
import mx.jalan.SHP.models.SubReporte;

public class Main {

	private static EntityManager manager;
	private static EntityManagerFactory emf;
	
	public static void main(String... args) {
		emf = Persistence.createEntityManagerFactory("app");
		manager = emf.createEntityManager();
		
		System.out.println("|-.-.-.-.-.-.-.= APP - STARTED =.-.-.-.-.-.-.-|");
		
		//Reporte 1 ----------------
		Reporte reporte = new Reporte();
		reporte.setReporte("Reporte #1");
		
		SubReporte subReporte = new SubReporte();
		subReporte.setReporte(reporte);
		subReporte.setSubReporteDescripcion("Descripción reporte #1");
		reporte.setSubReportes(Arrays.asList(subReporte));
		
		//Reporte 2 ----------------
		Reporte reporte1 = new Reporte();
		reporte1.setReporte("Reporte #2");
		
		SubReporte subReporte11 = new SubReporte();
		subReporte11.setReporte(reporte1);
		subReporte11.setSubReporteDescripcion("Descripción reporte #2 del subReporte #1");
		
		SubReporte subReporte12 = new SubReporte();
		subReporte12.setReporte(reporte1);
		subReporte12.setSubReporteDescripcion("Descripción reporte #2 del subReporte #2");
		
		reporte1.setSubReportes(Arrays.asList(subReporte11, subReporte12));
		
		//Reporte 3 ----------------
		Reporte reporte2 = new Reporte();
		reporte2.setReporte("Reporte #3");
		
		SubReporte subReporte21 = new SubReporte();
		subReporte21.setReporte(reporte2);
		subReporte21.setSubReporteDescripcion("Descripción reporte #3 del subReporte #1");
		
		SubReporte subReporte22 = new SubReporte();
		subReporte22.setReporte(reporte2);
		subReporte22.setSubReporteDescripcion("Descripción reporte #3 del subReporte #2");
		
		SubReporte subReporte23 = new SubReporte();
		subReporte23.setReporte(reporte2);
		subReporte23.setSubReporteDescripcion("Descripción reporte #3 del subReporte #3");
		
		reporte2.setSubReportes(Arrays.asList(subReporte21, subReporte22, subReporte23));
		
		manager.getTransaction().begin();
		manager.persist(reporte);
		manager.persist(reporte1);
		manager.persist(reporte2);
		manager.getTransaction().commit();
		
		Reporte findReporte = manager.find(Reporte.class, 1L);
		System.out.println(findReporte);
		manager.createQuery("SELECT r FROM Reporte r").getResultList().forEach(System.out::println);
	}
	
}
