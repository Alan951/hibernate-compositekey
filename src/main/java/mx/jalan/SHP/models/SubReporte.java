package mx.jalan.SHP.models;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;


@Entity
public class SubReporte {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idSubReporte;
	
	@ManyToOne
	@JoinColumn(name = "idReporte")
	private Reporte reporte;
	
	private String subReporteDescripcion;

	public SubReporte() {}

	public SubReporte(Long idSubReporte, Reporte reporte, String subReporteDescripcion) {
		super();
		this.idSubReporte = idSubReporte;
		this.reporte = reporte;
		this.subReporteDescripcion = subReporteDescripcion;
	}

	public Long getIdSubReporte() {
		return idSubReporte;
	}

	public void setIdSubReporte(Long idSubReporte) {
		this.idSubReporte = idSubReporte;
	}

	public Reporte getReporte() {
		return reporte;
	}

	public void setReporte(Reporte reporte) {
		this.reporte = reporte;
	}

	public String getSubReporteDescripcion() {
		return subReporteDescripcion;
	}

	public void setSubReporteDescripcion(String subReporteDescripcion) {
		this.subReporteDescripcion = subReporteDescripcion;
	}

	@Override
	public String toString() {
		return "SubReporte [idSubReporte=" + idSubReporte + ", subReporteDescripcion=" + subReporteDescripcion + "]";
	}
}
