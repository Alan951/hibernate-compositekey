package mx.jalan.SHP.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
public class Reporte {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idReporte")
	private Long idReporte;
	
	private String reporte;
	
	@OneToMany(mappedBy = "reporte", fetch =  FetchType.EAGER, cascade = CascadeType.ALL)
	private List<SubReporte> subReportes;
	
	public Reporte() {
		super();
	}

	public Reporte(Long idReporte, String reporte, List<SubReporte> subReportes) {
		super();
		this.idReporte = idReporte;
		this.reporte = reporte;
		this.subReportes = subReportes;
	}

	public Long getIdReporte() {
		return idReporte;
	}

	public void setIdReporte(Long idReporte) {
		this.idReporte = idReporte;
	}

	public String getReporte() {
		return reporte;
	}

	public void setReporte(String reporte) {
		this.reporte = reporte;
	}

	public List<SubReporte> getSubReportes() {
		return subReportes;
	}

	public void setSubReportes(List<SubReporte> subReportes) {
		this.subReportes = subReportes;
	}

	@Override
	public String toString() {
		return "Reporte [idReporte=" + idReporte + ", reporte=" + reporte + ", subReportes=" + subReportes + "]";
	}	
	
}
